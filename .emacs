(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(blink-cursor-mode nil)
 '(c-basic-offset 4)
 '(c-default-style "linux" t)
 '(c-offsets-alist (quote ((inline-open . 0))))
 '(custom-enabled-themes (quote (wombat)))
 '(delete-selection-mode nil)
 '(global-linum-mode t)
 '(inhibit-startup-screen t)
 '(mark-even-if-inactive t)
 '(scroll-bar-mode nil)
 '(show-paren-mode t)
 '(tab-width 4)
 '(tool-bar-mode nil)
 '(transient-mark-mode 1))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "DejaVu Sans Mono" :foundry "unknown" :slant normal :weight normal :height 98 :width normal))))
 '(linum ((t (:inherit (shadow default) :foreground "medium slate blue")))))
(put 'downcase-region 'disabled nil)

;; enable cua-mode
(cua-mode 1)
(tool-bar-mode 1)

(require 'package)
(add-to-list 'package-archives
             '("marmalade" . "http://marmalade-repo.org/packages/") t)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.milkbox.net/packages/") t)
(package-initialize)

(when (not package-archive-contents)
  (package-refresh-contents))

(defvar my-packages
  '(buffer-move magit
                smex company git-gutter
                autopair highlight-current-line
                json-reformat))

(dolist (p my-packages)
  (when (not (package-installed-p p))
	(package-install p)))

;;Title for frame
(setf frame-title-format "%b -- %f")

(server-start)

(global-linum-mode 1)

;;IDO mode stuff

(ido-mode t)
(setf ido-enable-flex-matching t)

(add-hook 'ido-setup-hook
          (lambda ()
            (define-key ido-completion-map [tab] 'ido-complete)))

(require 'smex)
(smex-initialize)

(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "M-X") 'smex-major-mode-commands)
;; This is your old M-x.
(global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)

;;END IDO stuff

(defun my-refresh-buffer ()
  (interactive)
  (revert-buffer t t))

(global-set-key [f5] 'my-refresh-buffer)


(defalias 'ff 'find-file)
(defalias 'ffw 'find-file-other-window)


(global-set-key (kbd "C-+") 'my-text-increase)
(global-set-key (kbd "C--") 'my-text-decrease)

(eshell)
(eshell-command "echo \"# Type 'magit-status' in a git directory to start.\"" 1)
(move-end-of-line nil)
